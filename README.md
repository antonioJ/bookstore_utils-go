# bookstore_oauth-go

Utils library for the Go programming language
## Authors and acknowledgment

This project is based on Udemy course: [How to design & develop REST microservices in Golang (Go)](https://www.udemy.com/course/golang-how-to-design-and-build-rest-microservices-in-go/) by [Federico León](https://federicoleon.com/)
